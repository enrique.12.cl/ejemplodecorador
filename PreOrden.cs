public class PreOrden : OrdenBase
{
    public override double CalcularTotalPrecioOrden()
    {
        Console.WriteLine("Calculo del precio total de un pedido anticipado");
        return productos.Sum(x => x.precio)*0.9;
    }
}